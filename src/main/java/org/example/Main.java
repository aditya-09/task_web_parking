package org.example;

import java.sql.*;

public class Main {

    // Menyiapkan paramter JDBC untuk koneksi ke datbase
    static final String JDBC_DRIVER = "com.mysql.cj.jdbc.Driver";
    static final String DB_URL = "jdbc:mysql://localhost:3306/java_parking?useUnicode=true&useJDBCCompliantTimezoneShift=true&useLegacyDatetimeCode=false&serverTimezone=UTC";
    static final String USER = "root";
    static final String PASS = "";

    // Menyiapkan objek yang diperlukan untuk mengelola database
    static Connection conn;
    static Statement stmt;
    static ResultSet rs;

    public void insert(){
        // harus dibungkus dalam blok try/catch
        try {
            // register driver yang akan dipakai
            Class.forName(JDBC_DRIVER);

            // buat koneksi ke database
            conn = DriverManager.getConnection(DB_URL, USER, PASS);

            // buat objek statement
            stmt = conn.createStatement();
            // buat query ke database
//            String insertSQL = "INSERT INTO parkCard (id_parkir, tipe) VALUES ("+123+", \"mobil\")";
//            stmt.executeUpdate(insertSQL);

            String sql = "SELECT * FROM parkCard";
            rs = stmt.executeQuery(sql);

            // tampilkan hasil query
            while(rs.next()){
                System.out.println("ID Parking: " + rs.getInt("id_parkir"));
                System.out.println("Jam Masuk: " + rs.getString("jam_masuk"));
            }

            stmt.close();
            conn.close();

        } catch (Exception e) {
            e.printStackTrace();
        }


    }



}
