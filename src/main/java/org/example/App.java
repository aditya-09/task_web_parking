package org.example;

import org.example.in.MainIn;
import org.example.out.MainOut;
import org.json.JSONObject;
import java.sql.*;
import java.util.Scanner;

import com.google.gson.Gson;


/**
 * Hello world!
 *
 */
public class App 
{
    static final String JDBC_DRIVER = "com.mysql.cj.jdbc.Driver";
    static final String DB_URL = "jdbc:mysql://localhost:3306/java_parking?useUnicode=true&useJDBCCompliantTimezoneShift=true&useLegacyDatetimeCode=false&serverTimezone=UTC";
    static final String USER = "root";
    static final String PASS = "";

    // Menyiapkan objek yang diperlukan untuk mengelola database
    static Connection conn;
    static Statement stmt;
    static ResultSet rs;


    public static void main( String[] args )
    {
        System.out.println( "Hello World!" );
        App a = new App();
        a.startApp();
//        a.getJSON(20063028);
//        a.parse();
    }

    public void startApp(){
        MainIn mIn = new MainIn();
        MainOut mOut = new MainOut();

        Scanner scan = new Scanner(System.in);

        System.out.print("IN/OUT \t: ");
        String get = scan.nextLine();

        if (get.equals("IN")){
            mIn.startIn();
        }else if(get.equals("OUT")){
            mOut.startOut(3123, "B2132");
        }else {
            System.out.println("Masukkan IN atau OUT");
        }
    }

    public void getJSON(Integer idparkir){
        try {
            Class.forName(JDBC_DRIVER);
            conn = DriverManager.getConnection(DB_URL, USER, PASS);
            stmt = conn.createStatement();

            String sql = "SELECT A.id_parkir, A.tipe, A.jam_masuk, B.jam_keluar, B.plat, B.biaya FROM parkcard A INNER JOIN parking B ON A.id_parkir=B.id_parkir WHERE A.id_parkir =%d";
            sql = String.format(sql, idparkir);
            rs = stmt.executeQuery(sql);

            // var to json
            int id = 0;
            String tipe = null;
            String jam_masuk = null;
            String jam_keluar = null;
            String plat = null;
            long biaya = 0;

            while (rs.next()){
                id = rs.getInt("id_parkir");
                tipe = rs.getString("tipe");
                jam_masuk = rs.getString("jam_masuk");
                jam_keluar = rs.getString("jam_keluar");
                plat = rs.getString("plat");
                biaya = rs.getInt("biaya");
            }
//            String jsonString = "{\"status_code\" : 200, \"status\" : \"success\", \"data\" : " +
//                    "{ \"id_parkir\" : "+id+", \"tipe\" : \""+tipe+"\", \"jam_masuk\" : "+jam_masuk+", " +
//                    "\"jam_keluar\" : "+jam_keluar+", \"plat nomor\" : \""+plat+"\", " +
//                    "\"biaya parkir\" : "+biaya+"} }";

            String jsonString = "{\"status_code\" : 200, \"status\" : \"success\", \"data\" : " +
                    "{ \"id_parkir\" : "+id+", \"tipe\" : \""+tipe+"\", \"jam_masuk\" : \""+jam_masuk+"\"} }";
            System.out.println(jsonString);

            JSONObject myResp = new JSONObject((jsonString));
            System.out.println(myResp);


        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void parse(){
//        JSONObject myobject= new JSONObject();
//        myobject.put("name", "Admin");
//        System.out.println(myobject.toString());

        String jsonString = "{\"name\" \n: \"admin\"}";
//        { "name" : "admin" }
        System.out.println(jsonString);
            JSONObject myResp = new JSONObject((jsonString));
        System.out.println(myResp);
    }
    

}
