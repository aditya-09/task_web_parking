package org.example.out;

import java.sql.*;
import java.util.Scanner;

public class MainOut extends Plat{
    static final String JDBC_DRIVER = "com.mysql.cj.jdbc.Driver";
    static final String DB_URL = "jdbc:mysql://localhost:3306/java_parking?useUnicode=true&useJDBCCompliantTimezoneShift=true&useLegacyDatetimeCode=false&serverTimezone=UTC";
    static final String USER = "root";
    static final String PASS = "";

    // Menyiapkan objek yang diperlukan untuk mengelola database
    static Connection conn;
    static Statement stmt;
    static ResultSet rs;

    //    dibutuhkan
    Integer idScan;
    String plat;
    Long price;
    String jam_masuk;
    String tipe;
    Integer idparkir = null;

    public void setScan(Integer id_parkir, String plat){
//        Scanner scan = new Scanner(System.in);

//        System.out.print("ID Parkir \t\t: ");
//        String idScan = scan.nextLine();
//        this.idScan = Integer.parseInt(idScan);
//        =============================
//        System.out.print("Tipe Kendaraan \t: ");
//        this.tipe = scan.nextLine();
//        ============================
//        System.out.print("Nomor Plat \t\t: ");
//        this.plat = scan.nextLine();

        this.idScan = id_parkir;
        this.plat = plat;
    }
    public void checkIdParking(){
        MainOut m = new MainOut();
        try {
            Class.forName(JDBC_DRIVER);
            conn = DriverManager.getConnection(DB_URL, USER, PASS);
            stmt = conn.createStatement();

            String sql = "SELECT * FROM parkCard WHERE id_parkir=%d";
            sql = String.format(sql, this.idScan);
            rs = stmt.executeQuery(sql);
            if (rs.next() == true){
                do {
                    this.idparkir = rs.getInt("id_parkir");
                    this.tipe = rs.getString("tipe");
                    this.jam_masuk = rs.getString("jam_masuk");
                }
                while (rs.next());
            }else {
                System.out.println("ID yang anda masukkan salah");
            }
            this.price = m.setPrice(this.jam_masuk,this.tipe);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void insertParking(){
        try {
            Class.forName(JDBC_DRIVER);
            conn = DriverManager.getConnection(DB_URL, USER, PASS);
            stmt = conn.createStatement();

            String insertSQL = "INSERT INTO parking (id_parkir, plat, biaya) VALUES ("+this.idparkir+",\""+this.plat+"\","+this.price+")";
            stmt.executeUpdate(insertSQL);
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        } catch (SQLException throwables) {
            throwables.printStackTrace();
        }

    }

    public void data(){
        try {
            String sql = "SELECT A.id_parkir, A.tipe, A.jam_masuk, B.jam_keluar, B.plat, B.biaya FROM parkcard A INNER JOIN parking B ON A.id_parkir=B.id_parkir WHERE A.id_parkir =%d";
            sql = String.format(sql, this.idparkir);
            rs = stmt.executeQuery(sql);
            while(rs.next()){
                System.out.println("ID Parking \t\t: " + rs.getInt("id_parkir"));
                System.out.println("Tipe Kendaraan \t: " + rs.getString("tipe"));
                System.out.println("Jam Masuk \t\t: " + rs.getString("jam_masuk"));
                System.out.println("Jam Keluar \t\t: " + rs.getString("jam_keluar"));
                System.out.println("Plat Nomor \t\t: " + rs.getString("plat"));
                System.out.println("Biaya Parkir \t: " + rs.getInt("biaya"));
            }
            stmt.close();
            conn.close();
        } catch (SQLException throwables) {
            throwables.printStackTrace();
        }
    }


    public void startOut(Integer id_park, String plat) {
        MainOut m = new MainOut();
        m.setScan(id_park, plat);
        m.checkIdParking();
        m.insertParking();
        System.out.println("\n====================================\n\t\t\t OUTPUT");
        m.data();
    }

    public static void main(String[] args) {
        MainOut m = new MainOut();
        m.startOut(20063019, "B31231");
    }
}
