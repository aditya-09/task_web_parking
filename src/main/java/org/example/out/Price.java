package org.example.out;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

public class Price {
    protected long price;
    public long setPrice(String in, String vehicle){
        Date date1 = null;
        Date date2 = null;
        String timeStamp = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(Calendar.getInstance().getTime());
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        try {
            date1 = sdf.parse(in);
            date2 = sdf.parse(timeStamp);
//            System.out.println(date2);
        } catch (Exception e) {
            System.out.println(e);
        }

        long time = date2.getTime() - date1.getTime();
        long hours = time / (60 * 60 * 1000);

//        long day = time / (24 * 60 * 60 * 1000);

//        tes
//        System.out.println(date1.getTime());
//        System.out.println(date2.getTime());
//        System.out.println(time);
//        System.out.println(hours);
//        System.out.println(day);

        int hasil = 0;
        if (vehicle.equals("motor")){
            if (hours == 0 ){
                hasil = 3000;
            }else {
                hasil = (int) (hours * 1000 + 3000);
            }
            return this.price = hasil;
        }else if(vehicle.equals("mobil")){
            if (hours == 0){
                hasil = 5000;
            }else {
                hasil = (int) (hours * 2000 + 5000);
            }
        }
        return this.price = hasil;
    }

//    public static void main(String[] args) {
//        Price p = new Price();
//        long harga =  p.setPrice("2020-06-30 05:22:38", "motor");
//        System.out.println(harga);
//    }
}
