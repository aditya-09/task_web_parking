package org.example.in;

import org.example.Main;

import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.Date;

public class IDParking extends Tipe{

    // dibutuhkan
    protected Integer idParking;

    public Integer setIdParking(){
        DateTimeFormatter myParse = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss");
        LocalDateTime now = LocalDateTime.now();
        String tanggal =  myParse.format(now);
        LocalDate Formatter = LocalDate.parse(tanggal, myParse);

//        set years month and date
        DateTimeFormatter Y = DateTimeFormatter.ofPattern("yy");
        DateTimeFormatter M = DateTimeFormatter.ofPattern("MM");
        DateTimeFormatter D = DateTimeFormatter.ofPattern("dd");


        String years = Formatter.format(Y);
        String month = Formatter.format(M);
        String date = Formatter.format(D);
        int random_int = (int)(Math.random() * (99 - 11 + 1) + 11);
        String park = years+month+date+ random_int;
        this.idParking = Integer.parseInt(park);
//        System.out.println(this.idParking);
        return this.idParking;
    }

//    public static void main(String[] args) {
//        IDParkir i = new IDParkir();
//        i.setIdPark();
//        Main m = new Main();
//        m.insert();
//    }
}
