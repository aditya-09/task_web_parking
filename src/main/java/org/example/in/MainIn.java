package org.example.in;

import org.json.JSONObject;

import java.util.Scanner;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.Statement;

public class MainIn extends IDParking {

    // Menyiapkan paramter JDBC untuk koneksi ke datbase
    static final String JDBC_DRIVER = "com.mysql.cj.jdbc.Driver";
    static final String DB_URL = "jdbc:mysql://localhost:3306/java_parking?useUnicode=true&useJDBCCompliantTimezoneShift=true&useLegacyDatetimeCode=false&serverTimezone=UTC";
    static final String USER = "root";
    static final String PASS = "";

    // Menyiapkan objek yang diperlukan untuk mengelola database
    static Connection conn;
    static Statement stmt;
    static ResultSet rs;

//    dibutuhkan
    String tipe;
    Integer idparkir;
    String jsonString;
    JSONObject json_data;

    public void setIn(){
        Scanner scan = new Scanner(System.in);
        MainIn m = new MainIn();

        System.out.print("Tipe Kendaraan \t: ");
        String tipeScan = scan.nextLine();
        this.tipe = m.setTipe(tipeScan);

        this.idparkir = m.setIdParking();

    }

    public void insertIn(String tipe){
        try {
            // register driver yang akan dipakai
            Class.forName(JDBC_DRIVER);

            // buat koneksi ke database
            conn = DriverManager.getConnection(DB_URL, USER, PASS);

            // buat objek statement
            stmt = conn.createStatement();
//            Integer idparkir = this.idparkir+8;
            MainIn m = new MainIn();
            this.idparkir = m.setIdParking();

            // buat query ke database
            String insertSQL = "INSERT INTO parkCard (id_parkir, tipe) VALUES ("+idparkir+",\""+tipe+"\")";
            stmt.executeUpdate(insertSQL);

            String sql = "SELECT * FROM parkCard WHERE id_parkir=%d";
            sql = String.format(sql, idparkir);

            rs = stmt.executeQuery(sql);

            // var to json
            int id = 0;
            String tipe_kendaraan = null;
            String jam_masuk = null;

            // tampilkan hasil query
            while(rs.next()){
                System.out.println("ID Parking \t\t: " + rs.getInt("id_parkir"));
                System.out.println("Tipe Kendaraan \t: " + rs.getString("tipe"));
                System.out.println("Jam Masuk \t\t: " + rs.getString("jam_masuk"));
                id = rs.getInt("id_parkir");
                tipe_kendaraan = rs.getString("tipe");
                jam_masuk = rs.getString("jam_masuk");
                this.jsonString = "{\"status_code\" : 200, \"status\" : \"success\", \"data\" : " +
                        "{ \"id_parkir\" : "+id+", \"tipe\" : \""+tipe_kendaraan+"\", \"jam_masuk\" : \""+jam_masuk+"\"} }";
            }

            stmt.close();
            conn.close();

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public JSONObject getJSON(){
        return this.json_data= new JSONObject((this.jsonString));
    }

    public void startIn(){
        MainIn m = new MainIn();
        m.setIn();
        System.out.println("\n==============================================\n\t\t\t INPUT");
        m.insertIn("motor");
    }

//    public static void main(String[] args) {
//        MainIn m = new MainIn();
//        m.insertIn("motor");
//    }

}
